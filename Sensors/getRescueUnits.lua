local sensorInfo = {
	name = "returns units to rescue",
	desc = "",
	author = "Lucia Todova",
	date = "13.5.2019",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local def = Spring.GetUnitDefID
local teamUnits = Spring.GetTeamUnits
local teamID = Spring.GetMyAllyTeamID
local pos = Spring.GetUnitPosition

-- compare function for sort
function compare(a,b)
    xa,ya,za = pos(a)
    xb,yb,zb = pos(b)
    return za > zb
end

return function(unloadDestination)
	local allUnits = teamUnits(teamID())
	local rescueUnits = {}

	--separate rescuable units
	for i = 1, #allUnits
	do
		currentDef = def(allUnits[i])
		if UnitDefs[currentDef].humanName == "Hatrack" or UnitDefs[currentDef].humanName == "Hammer" or
			UnitDefs[currentDef].humanName == "Box-of-Death" or UnitDefs[currentDef].humanName == "Bulldog" or 
			UnitDefs[currentDef].humanName == "Rocko" then
				rescueUnits[#rescueUnits + 1] = allUnits[i]
		end
	end
	--sort by z to get the closest
	table.sort(rescueUnits, compare)

	return rescueUnits
end