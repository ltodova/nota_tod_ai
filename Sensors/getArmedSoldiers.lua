local sensorInfo = {
	name = "returns armed units from selected",
	desc = "",
	author = "Lucia Todova",
	date = "13.5.2019",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetUnitDef = Spring.GetUnitDefID
local SpringGetSelectedUnits = Spring.GetSelectedUnits

return function()
	--only armed soldiers
    armedSoldiers = {}
    selectedUnits = SpringGetSelectedUnits()
	for i = 1,#selectedUnits do
		if UnitDefs[SpringGetUnitDef(selectedUnits[i])].isTransport == false then
			armedSoldiers[#armedSoldiers + 1] = selectedUnits[i]
		end
	end

	return armedSoldiers
end