local sensorInfo = {
	name = "return all hills on map",
	desc = "",
	author = "Lucia Todova",
	date = "13.5.2019",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetGroundHeight = Spring.GetGroundHeight

return function()
    hills = {}
    local i = 0
    
	--find the hills
	while i < Game.mapSizeX do
		for j=0,Game.mapSizeZ,10 do
			h = SpringGetGroundHeight(i,j)
			--ground height is 128 on this map
			if h > 128 then
				i = i + 10
				newH = SpringGetGroundHeight(i,j)
				while newH > h do
					i = i + 10
					h = newH
					newH = SpringGetGroundHeight(i,j)
				end
				hills[#hills+1] = Vec3(i, newH, j+40)
				while SpringGetGroundHeight(i,j) > 128 do
					i = i + 10
				end
				break
			end
		end
		i = i + 10
	end

	return hills
end