local sensorInfo = {
	name = "WindDirection",
	desc = "Return data of actual wind.",
	author = "Lucina",
	date = "2019-04",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetWind = Spring.GetWind

-- @description return current wind statistics
return function()
	local dirX, dirY, dirZ, strength, normDirX, normDirY, normDirZ = SpringGetWind()
	return {
		dirX = -normDirX*strength,
		dirZ = -normDirZ*strength,
	}
end