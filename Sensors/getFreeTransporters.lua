local sensorInfo = {
	name = "currentFormation",
	desc = "create formation for currently selected units",
	author = "Lucia Todova",
	date = "13.5.2019",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local def = Spring.GetUnitDefID
local teamUnits = Spring.GetTeamUnits
local active = Spring.GetUnitIsActive
local teamID = Spring.GetMyAllyTeamID

return function()
	local allUnits = teamUnits(teamID())
	local idleTransporters = {}

	--separate transporters
	for i = 1, #allUnits
	do
		currentDef = def(allUnits[i])
		if UnitDefs[currentDef].humanName == "Atlas" and active(allUnits[i]) == false then
                idleTransporters[#idleTransporters + 1] = allUnits[i]
		end
	end

	return idleTransporters
end