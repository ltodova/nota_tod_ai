function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Find hills on map and move armed units on them",
		parameterDefs = {
			{ 
				name = "soldiers",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "<soldiers to use>",
            },
            { 
				name = "hills",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "<hills to get to",
			}
		}
	}
end

-- speed-ups
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

function Run(self, units, parameter)
	local armedSoldiers = parameter.soldiers
	local hills = parameter.hills
	
	--send the soldiers
	for i=1,#hills do
		--no more soldiers
		if i > #armedSoldiers then
			break
		end
		SpringGiveOrderToUnit(armedSoldiers[i],CMD.MOVE,hills[i]:AsSpringVector(), {})
	end

	return RUNNING
end


function Reset(self)
	return self
end