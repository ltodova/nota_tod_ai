function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Rescue units",
		parameterDefs = {
			{ 
				name = "destination",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "<safe destination area>",
            },
            { 
				name = "rescueUnits",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "<units to rescue>",
			},
            { 
				name = "freeTransporters",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "<idle transporters>",
			}
		}
	}
end

-- speed-ups
local order = Spring.GiveOrderToUnit

--order transporter to load a unit, move to unload destination, unload unit and move away
function orderTransporter(transporter, currentUnit, unloadDestination)
    order(transporter, CMD.LOAD_UNITS, {currentUnit}, {})
    order(transporter, CMD.MOVE, Vec3(unloadDestination[1],unloadDestination[2],unloadDestination[3]):AsSpringVector(), {"shift"})
    order(transporter, CMD.UNLOAD_UNITS, unloadDestination, {"shift"})
    order(transporter, CMD.MOVE, Vec3(unloadDestination[1],unloadDestination[2],unloadDestination[3]+2*unloadDestination[4]):AsSpringVector(), {"shift"})
end

function Run(self, units, parameter)
    local destination = parameter.destination;
    local freeTransporters = parameter.freeTransporters
    local rescueUnits = parameter.rescueUnits 

    while #rescueUnits > 0 do
        --we want 2 transporters for each unit in case one gets destroyed
        if #freeTransporters > 1 then
            freeTransporter1 = freeTransporters[#freeTransporters]
            freeTransporter2 = freeTransporters[#freeTransporters-1]
        else
            break
        end

        freeTransporters[#freeTransporters] = nil
        freeTransporters[#freeTransporters] = nil

        --save current unit to rescue
        currentUnit = rescueUnits[#rescueUnits]
        rescueUnits[#rescueUnits] = nil

        unloadDestination = {destination["center"]["x"],destination["center"]["y"],destination["center"]["z"],destination["radius"]}

        orderTransporter(freeTransporter1,currentUnit,unloadDestination)
        orderTransporter(freeTransporter2,currentUnit,unloadDestination)
    end

    return RUNNING
end


function Reset(self)
end
